# The-viewport-component
The viewport component
Q.register('viewport' ,{
   added: function() {
	this.entity.bind('predraw', this, 'predraw');
        this.entity.bind('draw',this,'postdraw');
        this.x = 0 ;	   
	this.y = 0;
	this.centerX = Q.width/2;
	this.centerY = Q.height/2;
	this.scale = 1;			   	   
    },	
	
    extend: {
         follow: function(sprite) {
             this.unbind('step', this.viewport);
	     this.viewport.following = sprite;
	     this.bind('step',this.viewport,'follow');
	     this.viewport.follow();
	},
	
	unfollow:  function() 
	  this.unbind('step',this.viewport);
	},
	
	centerOn: function(x,y) }
	   this.viewport.centerOn(x,y);
	}
     },
    
    follow: function() { 
        this.centerOn(this.following.p.x + this.following.p.w/2,
	                       this.following.p.y + this.following.p.h/2 );
    },     
	
    centerOn: function(x,y) {
	this.centerX = x;
	this.centerY = y;
	
	this.x = this.centerX = Q.width / 2 / this.scale;
	this.y = this.centerY = Q.height / 2 / this.scale;
   },  
   
   postdraw: function() {
	Q.ctx.restore();
	}
});
	
